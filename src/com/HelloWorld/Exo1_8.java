package com.HelloWorld;

import java.util.Scanner;

public class Exo1_8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Début
		try (Scanner scanner = new Scanner(System.in)){
			System.out.print("Entrez un premier nombre : ");
			int n1 =scanner.nextInt();
			
			System.out.print("Entrez un deuxième nombre : ");
			int n2 = scanner.nextInt();
		if (n1 > 0 && n2 > 0) {
			System.out.print("Le produit est positif !");
		}else if (n1 == 0 || n2 == 0) {
			System.out.print("Le produit est nul !");
		}else {
			System.out.print("Le produit est négatif !");
			}
		}
	}
}
