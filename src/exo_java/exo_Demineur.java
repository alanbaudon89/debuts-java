package exo_java;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.BorderLayout;

 

@SuppressWarnings("serial")
//Spécifier que exo_démineur hérite de JFrame (extends)
//Ajouter les méthodes dans les interface actionlistener et MouseListener
public class exo_Demineur extends JFrame implements ActionListener,
        MouseListener {
	//Variables ligne, colonne et  nombre de mines
    int rows = 10;
    int cols = 10;
    int numMines = 10;
    //On utilise gridLayout pour organsier les composant en grille rectangulaire
    GridLayout layout = new GridLayout(rows, cols);
    
    
    
    
   
    
   //Variable en booléen
    boolean[] mines = new boolean[rows * cols];
    boolean[] clickable = new boolean[rows * cols];
    boolean lost = false;
    boolean won = false;
    int[] numbers = new int[rows * cols];
    //création des boutons composant la grille
    JButton[] buttons = new JButton[rows * cols];
    
    boolean[] clickdone = new boolean[rows * cols];
    //création du menu d'option avec Jmenu
    JMenuItem newGameButton = new JMenuItem("Nouvelle partie");
    JMenuItem difficulty = new JMenuItem("options");
    JLabel mineLabel = new JLabel("mines: " + numMines + " marked: 0");
    JPanel p = new JPanel();
    Icon mine = new ImageIcon(getClass().getResource("/pit.png"));
    
    
    
  
    
    
    
    public exo_Demineur() { 
    	//test pour fixer les dimensions du démineur ( infructeux, probablement gridlayout ) 
    	JFrame frame = new JFrame();
    	frame.setSize(new Dimension(400,400));
    	p.setLayout(layout);
    	frame.setContentPane(p);    	
    	frame.setLayout(layout);
    	frame.setResizable(false);
        this.setTitle("Démineur");
        //p.setMaximumSize(new Dimension(40,40));
        
        
        //appelle de la méthode setupI 
        setupI();
        //Mise en positions des boutons en fonction des ligne et colonnes
        for (int i = 0; i < (rows * cols); i++) {
            p.add(buttons[i]);
        }
        //Ajout de la barre de menu quicontiendra les Jmenuitem
        JMenuBar mb = new JMenuBar();
        JMenu m = new JMenu("paramètres");
        //enregistre le composant avec le listener
        newGameButton.addActionListener(this);
        m.add(newGameButton);
        difficulty.addActionListener(this);
        m.add(difficulty);
        mb.add(m);
        this.setJMenuBar(mb);
        this.add(p);
        this.add(mineLabel, BorderLayout.SOUTH);
        this.pack();
        this.setVisible(true);
    }
    	//méthode remplissant aléatoirement les mines 
    public void fillMines() {
        int needed = numMines;
        //tant que le nombre de mines à poser est supérieur à 0
        while (needed > 0) {
        	//disposer aléatoirement des mines
            int x = (int) Math.floor(Math.random() * rows);
            int y = (int) Math.floor(Math.random() * cols);
            if (!mines[(rows * y) + x]) {
                mines[(rows * y) + x] = true;
            //on décrémente needed pour arriver à 0 mines restantes à poser    
                needed--;
            }
        }
    }
    		//méthode pour faire apparaître le chiffre indiquant le nombre de miens aux alentours de cette case
    public void fillNumbers() {
    	//Pour chaque case, vérifie chaque case adjacentes, si mines incrémente le chiffre compteur qui apparaitra
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                int cur = (rows * y) + x;
                if (mines[cur]) {
                    numbers[cur] = 0;
                    continue;
                }
                int temp = 0;
                boolean l = (x - 1) >= 0;
                boolean r = (x + 1) < rows;
                boolean u = (y - 1) >= 0;
                boolean d = (y + 1) < cols;
                int left = (rows * (y)) + (x - 1);
                int right = (rows * (y)) + (x + 1);
                int up = (rows * (y - 1)) + (x);
                int upleft = (rows * (y - 1)) + (x - 1);
                int upright = (rows * (y - 1)) + (x + 1);
                int down = (rows * (y + 1)) + (x);
                int downleft = (rows * (y + 1)) + (x - 1);
                int downright = (rows * (y + 1)) + (x + 1);
                if (u) {
                    if (mines[up]) {
                        temp++;
                    }
                    if (l) {
                        if (mines[upleft]) {
                            temp++;
                        }
                    }
                    if (r) {
                        if (mines[upright]) {
                            temp++;
                        }
                    }
                }
                if (d) {
                    if (mines[down]) {
                        temp++;
                    }
                    if (l) {
                        if (mines[downleft]) {
                            temp++;
                        }
                    }
                    if (r) {
                        if (mines[downright]) {
                            temp++;
                        }
                    }
                }
                if (l) {
                    if (mines[left]) {
                        temp++;
                    }
                }
                if (r) {
                    if (mines[right]) {
                        temp++;
                    }
                }
                numbers[cur] = temp;
            }
        }
    }
 
    public void setupI() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                mines[(rows * y) + x] = false;
                clickdone[(rows * y) + x] = false;
                clickable[(rows * y) + x] = true;
                buttons[(rows * y) + x] = new JButton();
                //Nouveau test de dimensions des composants
                //buttons[(rows * y) + x].setSize(40,40);
                //Dimension maxSize = new Dimension(50, 50);
                buttons[(rows * y) + x].setPreferredSize(new Dimension(45, 45));
                buttons[(rows * y) + x].setMaximumSize(new Dimension(50,50));
                //enregistre les composants avec les listener
                buttons[(rows * y) + x].addActionListener(this);
                buttons[(rows * y) + x].addMouseListener(this);
            }
        }
        //appelle des  méthodes
        fillMines();
        fillNumbers();
    }
    
    public void setupI2() {
        this.remove(p);
        p = new JPanel();
        layout = new GridLayout(rows, cols);
        p.setLayout(layout);
        buttons = new JButton[rows * cols];
        mines = new boolean[rows * cols];
        clickdone = new boolean[rows * cols];
        clickable = new boolean[rows * cols];
        numbers = new int[rows * cols];
        setupI();
        for (int i = 0; i < (rows * cols); i++) {
            p.add(buttons[i]);
        }
        this.add(p);
        this.pack();
        fillMines();
        fillNumbers();
    }
 
    public void setup() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                mines[(rows * y) + x] = false;
                clickdone[(rows * y) + x] = false;
                clickable[(rows * y) + x] = true;
                buttons[(rows * y) + x].setEnabled(true);
                buttons[(rows * y) + x].setText("");
                buttons[(rows * y) + x].setSize(40,40);
            }
        }
        fillMines();
        fillNumbers();
        lost = false;
        mineLabel.setText("mines: " + numMines + " marquée: 0");
    }
 
    public static void main(String[] args) {
        new exo_Demineur();
       
        
    }
    //appelle la méthode à éxécuter actionperformed avec comme apramètre l'instance de la classe Actionevent construite
    public void actionPerformed(ActionEvent e) {
    	//get source donne référence à l'(objet dont l'évenement est issu
        if (e.getSource() == difficulty) {
        	//permet de faire apparaitre des messages *3
            rows = Integer.parseInt((String) JOptionPane.showInputDialog(
                    this, "Rows", "Rows", JOptionPane.PLAIN_MESSAGE, null,
                    null, 10));
            cols = Integer.parseInt((String) JOptionPane.showInputDialog(
                    this, "Columns", "Columns", JOptionPane.PLAIN_MESSAGE,
                    null, null, 10));
            numMines = Integer.parseInt((String) JOptionPane.showInputDialog(this, "Mines", "Mines",
                    JOptionPane.PLAIN_MESSAGE, null, null, 10));
            setupI2();
        }
        if (!won) {
            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    if (e.getSource() == buttons[(rows * y) + x]
                            && !won && clickable[(rows * y) + x]) {
                        doCheck(x, y);
                        break;
                    }
                }
            }
        }
        if (e.getSource() == newGameButton) {
            setup();
            won = false;
            return;
 
        }
        checkWin();
    }
    //appelle la méthode quand la souris est cliquée sur un composant (ici jbutton)
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == 3) {
            int n = 0;
            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    if (e.getSource() == buttons[(rows * y) + x]) {
                        clickable[(rows * y) + x] = !clickable[(rows * y)
                                + x];
                    }
                    if (!clickdone[(rows * y) + x]) {
                        if (!clickable[(rows * y) + x]) {
                            buttons[(rows * y) + x].setText("X");
                            n++;
                        } else {
                            buttons[(rows * y) + x].setText("");
                        }
                        mineLabel.setText("mines: " + numMines + " marked: "
                                + n);
                    }
                }
            }
        }
    }
 
    
    public void doCheck(int x, int y) {
        int cur = (rows * y) + x;
        boolean l = (x - 1) >= 0;
        boolean r = (x + 1) < rows;
        boolean u = (y - 1) >= 0;
        boolean d = (y + 1) < cols;
        int left = (rows * (y)) + (x - 1);
        int right = (rows * (y)) + (x + 1);
        int up = (rows * (y - 1)) + (x);
        int upleft = (rows * (y - 1)) + (x - 1);
        int upright = (rows * (y - 1)) + (x + 1);
        int down = (rows * (y + 1)) + (x);
        int downleft = (rows * (y + 1)) + (x - 1);
        int downright = (rows * (y + 1)) + (x + 1);
 
        clickdone[cur] = true;
        buttons[cur].setEnabled(false);
        if (numbers[cur] == 0 && !mines[cur] && !lost && !won) {
            if (u && !won) {
                if (!clickdone[up] && !mines[up]) {
                    clickdone[up] = true;
                    buttons[up].doClick();
                }
                if (l && !won) {
                    if (!clickdone[upleft] && numbers[upleft] != 0
                            && !mines[upleft]) {
                        clickdone[upleft] = true;
                        buttons[upleft].doClick();
                    }
                }
                if (r && !won) {
                    if (!clickdone[upright] && numbers[upright] != 0
                            && !mines[upright]) {
                        clickdone[upright] = true;
                        buttons[upright].doClick();
                    }
                }
            }
            if (d && !won) {
                if (!clickdone[down] && !mines[down]) {
                    clickdone[down] = true;
                    buttons[down].doClick();
                }
                if (l && !won) {
                    if (!clickdone[downleft] && numbers[downleft] != 0
                            && !mines[downleft]) {
                        clickdone[downleft] = true;
                        buttons[downleft].doClick();
                    }
                }
                if (r && !won) {
                    if (!clickdone[downright]
                            && numbers[downright] != 0
                            && !mines[downright]) {
                        clickdone[downright] = true;
                        buttons[downright].doClick();
                    }
                }
            }
            if (l && !won) {
                if (!clickdone[left] && !mines[left]) {
                    clickdone[left] = true;
                    buttons[left].doClick();
                }
            }
            if (r && !won) {
                if (!clickdone[right] && !mines[right]) {
                    clickdone[right] = true;
                    buttons[right].doClick();
                }
            }
        } else {
            buttons[cur].setText("" + numbers[cur]);
            if (!mines[cur] && numbers[cur] == 0) {
                buttons[cur].setText("");
            }
        }
        if (mines[cur] && !won) {
            buttons[cur].setIcon(mine);
            doLose();
        }
    }
    //vérifie la conditions de victoire
    public void checkWin() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                int cur = (rows * y) + x;
                if (!clickdone[cur]) {
                    if (mines[cur]) {
                        continue;
                    } else {
                        return;
                    }
                }
            }
        }
     //Appelle la méthode de victoire
        doWin();
    }
    // méthode de victoire mettant fin à la partie avec un messageet une option pour recommecner la partie
    public void doWin() {
        if (!lost && !won) {
            won = true;
            JOptionPane.showMessageDialog(null,
                    "gagné ! Nouvelle partie ?", "Victory",
                    JOptionPane.INFORMATION_MESSAGE);
            newGameButton.doClick();
        }
    }
    //Méthode de défaite vérifie sir la condition de défaite est remplie et envoei le message adéquate
    public void doLose() {
        if (!lost && !won) {
            lost = true;
            for (int i = 0; i < rows * cols; i++) {
                if (!clickdone[i]) {
                    buttons[i].doClick(0);
                }
            }
            JOptionPane.showMessageDialog(null,
                    "Perdu, commence une nouvelle partie", "Looser",
                    JOptionPane.ERROR_MESSAGE);
            setup();
        }
    }
//méthodes auto-générées
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
