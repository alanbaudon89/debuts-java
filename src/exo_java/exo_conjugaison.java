package exo_java;

import java.util.Scanner;

public class exo_conjugaison {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Donnez un verbe du premier groupe ");
	    String chaine = sc.next();
		final String[] sujets = {"je", "Tu","il/Elle","Nous","Vous","Ils/Elles"};
		final String[] terminaisons = {"e","es","e","ons","ez","ent"};
		String radical = chaine.substring(0, chaine.length()-2);
		String fin = chaine.substring(chaine.length()-2,chaine.length());
		
		if(fin.equals("er")) {
			for (int i=0;i<sujets.length;i++) {
				System.out.println(sujets[i]+" "+radical+terminaisons[i]);
			}	
		}
		else {
			System.err.println("Ce n'est pas un verbe du premier groupe !");
		}
	}
}
