package exo_java;

import java.util.Scanner;

public class exo_puissance {
	
	public static void main(String[] arg) {

		Scanner scanner = new Scanner(System.in);
		
		//alignement requis
		int N = 4;
		// colonnes et lignes
		int C = 7;
		int L = 6;
		//création plateau
		char[][] plateau = new char[C][L];
		//remplir de vide
		for (int x = 0; x<C ; x++)
			for ( int y = 0; y < L;y++)
				plateau[x][y] = '.';
		
		int gagnant = 0;
		
		for(int i = 1; i<=C*L; i++) {
		System.out.println("Tour "+ i + ", Etat du plateau :");
		
		for(int loop = 0; loop<C+2+2*C; loop++)
			System.out.print('-');
			System.out.println();
			
		for(int y=0; y<L;y=y+1) {
			System.out.print('|');
			for(int x=0;x<C;x++) {
				System.out.print(" " + plateau[x][y]+ " ");
			}
			System.out.print('|');
			System.out.println();
		}
		for(int loop = 0; loop<C+2+2*C;loop++)
			System.out.print('.');
			System.out.println();
			
			System.out.println("Tour du joueur "+ (i%2==1 ? 'X': 'O'));
			System.out.println("Entrez le numéro de la colonne entre 1 et "+ C +"...");
			boolean placement = false;
			int colonne = -1;
			
			while(!placement) {
				colonne = -1;
				String ligne = scanner.nextLine();
				try {
					colonne = Integer.valueOf(ligne);
					if(colonne >= 1 && colonne <= C) {
						if(plateau[colonne - 1][0] != '.') {
							System.out.println("Colonne pleine, réitérez");
						}else {
							placement  = true;
						}
					}else {
						System.out.println("Nombre incorrect, réitérez");
					}
				}catch(Exception e) {System.out.println("Nombre incorrect, réitérez");}
			}
			// Placement du jeton : 
			int rang = L-1;
			while(plateau[colonne - 1][rang]!='.') {
				rang--;
			}
			plateau[colonne - 1][rang] = (i%2==1 ? 'X' : 'O');
			
			// Check victoire
			
			//Symbole en cours : 
			char symbole = ( i%2==1 ? 'X' : 'O');
			//nombre aligné maximal :
			int max = 0;
			int x;int y;
			int somme;
			
			// diagonale HG-BD
			x=colonne-1; y = rang; somme=-1;
			while(y>=0 && x>=0 && plateau[x][y] == symbole) {y--; x--; somme++;}
			x = colonne-1; y = rang;
			while(y < L && x < C && plateau[x][y] == symbole ) {y++;x++;somme++;}
			if(somme > max) max = somme;
			
			//diagonale HD-BG
			x = colonne-1; y = rang; somme=-1;
			while(y >= 0 && x < C && plateau[x][y] == symbole){ y--; x++; somme++;}
			x = colonne-1; y = rang;
			while(y < L && x >= 0 && plateau[x][y] == symbole){ y++; x--; somme++;}
			if(somme > max) max= somme;
			
			//verticale
			x=colonne-1; y = rang; somme=-1;
			while(y>=0 && plateau[x][y] == symbole) {y--; somme++;}
			y = rang;
			while(y < L && plateau[x][y] == symbole ) {y++;somme++;}
			if(somme > max) max = somme;
			
			//horizontale
			x=colonne-1; y = rang; somme=-1;
			while(x >=0 && plateau[x][y] == symbole) {x--; somme++;}
			x = colonne-1;
			while(x < C && plateau[x][y] == symbole ) {x++;somme++;}
			if(somme > max) max = somme;
			
			if(max >= N) {
				gagnant = (i%2==1 ? 1 : 2);
				i = C*L+1;
			}
			
			System.out.println("*******Tour Suivant*******");
		}
		
		System.out.println();
		System.out.println("**************");
		System.out.println("*******Fin de partie*******");
		System.out.println("**************");
		if(gagnant == 0)
			System.out.println("***Egalité***");
		if(gagnant == 1)
			System.out.println("***Victoire de X***");
		if(gagnant == 2)
			System.out.println("***Victoire de O***");
		System.out.println("**************");
	}
}