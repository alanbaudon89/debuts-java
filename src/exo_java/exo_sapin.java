package exo_java;

import java.util.Scanner;

public class exo_sapin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//lire la saisie, import L3 du scanner,
		Scanner scanner = new Scanner(System.in);
		//On demande le nombre de lignes souhaités
		System.out.println("Combien de lignes ? ");
		int rows =scanner.nextInt();
        System.out.println("\n2. Sapin complet\n");
        //boucle pour placer les espaces par ligne
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows - i; j++) {
                System.out.print(" ");
            }
            //boucle pour placer les étoiles
            for (int k = 0; k <= i; k++) {
                System.out.print("* ");
            }
            System.out.println("");
        }
        
    }
}
	


