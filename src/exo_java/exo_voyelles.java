package exo_java;

import java.util.Scanner;

public class exo_voyelles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner sc = new Scanner(System.in);
	        int comp[] = new int[6];
	      //Lire la saisie du mot
	        System.out.print("Donnez un mot : ");
	        String mot = sc.next();
	      //passage de la string en minuscule pour réduire les cas du switch
	        System.out.println(mot.toLowerCase());
	      //chaque lettre du mot est checké puis les valeurs sont ajoutées aux compteurs
	        for (int i = 0; i < mot.length(); i++) {
	            switch (mot.charAt(i)) {
	                case 'a':
	                    comp[0]++;
	                    break;
	                case 'e':
	                    comp[1]++;
	                    break;
	                case 'i':
	                    comp[2]++;
	                    break;	                
	                case 'o':
	                    comp[3]++;
	                    break;	                
	                case 'u':
	                    comp[4]++;
	                    break;	                
	                case 'y':
	                    comp[5]++;
	                    break;
	        }
	        }
	        System.out.println(comp[0] + " fois la lettre a");
	        System.out.println(comp[1] + " fois la lettre e");
	        System.out.println(comp[2] + " fois la lettre i");
	        System.out.println(comp[3] + " fois la lettre o");
	        System.out.println(comp[4] + " fois la lettre u");
	        System.out.println(comp[5] + " fois la lettre y");
	}
}
